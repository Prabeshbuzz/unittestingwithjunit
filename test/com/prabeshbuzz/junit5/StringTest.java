package com.prabeshbuzz.junit5;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Duration;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class StringTest {
	
	
	//adding this comment to this new feature branch//
	
	@AfterAll
	static void afterAll() {
		System.out.println("Closing Connection to db");
	}
	
	@BeforeEach
	void beforeEach(TestInfo info) {
		System.out.println("Initializing test data for "+info.getDisplayName());
	}
	
	@AfterEach
	void afterTestingData(TestInfo info) {
		System.out.println("Closing connection pool for "+info);
	}
	
	@Test
	void stringLength() {
		int actualLength ="ABCD".length();
		int expectedLength = 4;
		
		assertEquals(expectedLength,actualLength);
		
	}

	@Test
	void toUpperCase() {
		String str="abcd";
		String result=str.toUpperCase();
		boolean emp = true;
		assertEquals("ABCD",result);
	}
	
	@Test
	void spliArray() {
		String str= "abc deg ghi";
		String actualResult[] = str.split(" ");
		String expectedResult[] = {"abc","deg","ghi"};
				assertArrayEquals(expectedResult,actualResult);
		
	}
	
	@Test
	@DisplayName("When length is null throw an exception")
	 void whenExceptionThrown_thenAssertionSucceeds() {
	    String test = null;
	    assertThrows(NullPointerException.class, () -> {
	        test.length();
	    });
	 }
	
	@ParameterizedTest
	 @ValueSource(strings= {"abc","def","ghi"})
	 void greaterthanzero(String stri) {
		 assertTrue(stri.length()>0);
	}
	
	@ParameterizedTest
	@CsvSource(value= {"abc,ABC","def,DEF"})
	void capTestWithCsv(String word,String capWord) {
		assertEquals(capWord,word.toUpperCase());
	}

	@ParameterizedTest(name= "{0} length is {1}")
	@CsvSource(value= {"ram,3","hari,4","shyam,5"})
	void testLength(String name,int length) {
		assertEquals(length,name.length());
	}
	
	//Performance test
	@Test
	@DisplayName("Performance test for 5 sec")
	void performanceTest() {
		assertTimeout(Duration.ofSeconds(5), ()->{
											for(int i =1;i<1000000000;i++) {
												System.out.println(i);
											}
											});
	}
	@Nested
	class nestedStringTest {
		String str;
		@BeforeEach
		void setToEmpty() {
			 str="";
		}
		@Test
		void lengthisZero() {
			assertEquals(0, str.length());
		}
		
		@Test
		void upperCaseEmpty() {
			assertEquals("", str.toUpperCase());
		}
		
	}
}
	

